//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package net.kenzan.helloworld.domain.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.netflix.config.DynamicIntProperty;
import com.netflix.config.DynamicPropertyFactory;
import net.kenzan.helloworld.domain.entities.DomainResultCode;
import net.kenzan.helloworld.domain.entities.DomainResultCode;
import net.kenzan.helloworld.domain.entities.Person;
import net.kenzan.helloworld.domain.security.context.DomainServiceException;
import net.kenzan.helloworld.domain.helpers.TransactionHelper;
import net.kenzan.helloworld.domain.responses.FetchPeopleResponse;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PeopleApiV2 {
    private static final DynamicIntProperty CONNECT_TIMEOUT = new DynamicIntProperty("client.api.connectTimeout.milliseconds", 10000);
    private static final DynamicIntProperty READ_TIMEOUT = new DynamicIntProperty("client.api.readTimeout.milliseconds", 15000);

    private static final Logger LOGGER = LoggerFactory.getLogger(PeopleApiV2.class);

    private static final String HOST_URL = DynamicPropertyFactory.getInstance()
            .getStringProperty("helloworld.domain.host.url", "").get();



    public PeopleApiV2 () {
    }

    public Observable<FetchPeopleResponse> queryPeopleObservable(String userId) {
        return Observable.fromCallable(() -> {
            return this.queryPeople(userId);
        });
    }

    public FetchPeopleResponse queryPeople (String userId) {
        String path = "/helloworld-test-service/people/" + userId;
        String txnId = TransactionHelper.getTransactionHeader();

        if(userId.isEmpty() || userId == null) {
            throw new DomainServiceException("Failed due to userId null or empty", DomainResultCode.BAD_REQUEST);
        }

        HttpClient client = new DefaultHttpClient();
        String absolutUrl = HOST_URL + path;
        LOGGER.info("Absolut url request: [{}]", absolutUrl);
        HttpGet getPeopleRequest = new HttpGet(absolutUrl);

        try {
            // Perform the request and chec status code
            HttpResponse response = client.execute(getPeopleRequest);
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() != 200) {
                throw new DomainServiceException("Failed due to statusCode: " + statusLine.getStatusCode(), DomainResultCode.fromIntValue(statusLine.getStatusCode()));
            }
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            try {
                // Read the server response and attempt to parse it as Json
                InputStreamReader reader = new InputStreamReader(content);

                Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,
                        (JsonDeserializer<LocalDate>) (JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) -> {
                            return LocalDate
                                    .parse(json.getAsJsonPrimitive().getAsString(),
                                            DateTimeFormatter.ISO_LOCAL_DATE);
                        }).create();

                List<Person> people = new ArrayList<>();
                JsonObject job = gson.fromJson(reader, JsonObject.class);
                JsonArray jsonArray = job.getAsJsonArray("people");
                LOGGER.info("clientResponse in command: {}", jsonArray);

                for (JsonElement element : jsonArray) {
                    people.add(gson.fromJson(element, Person.class));
                }

                return new FetchPeopleResponse.Builder()
                        .withCode(DomainResultCode.SUCCESS)
                        .withPeople(people)
                        .withTxnId(txnId)
                        .build();

            } catch (Exception ex) {
                LOGGER.error("Failed to parse JSON due to:", ex);
                throw new DomainServiceException("Failed to parse JSON", ex, DomainResultCode.SERVER_ERROR);
            } finally {
                content.close();
            }

        } catch (DomainServiceException ex) {
            LOGGER.error("Error in the command", ex);
            throw new DomainServiceException(ex.getMessage(), ex, ex.getDomainResultCode());
        } catch (Exception ex) {
            LOGGER.error("Error in the command", ex);
            throw new DomainServiceException("Error in the command: ", ex, DomainResultCode.SERVER_ERROR);
        }


    }

}
