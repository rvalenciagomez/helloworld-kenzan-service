package net.kenzan.helloworld.domain.api;

import net.kenzan.helloworld.domain.entities.Person;

import javax.ws.rs.core.Response;

public interface V1ApiDelegate {

    /**
     * @param xDomainSessionIdentity
     * @return Response
     */
    Response getPeople(String xDomainSessionIdentity, String userId);

    /**
     * @param person
     * @return Response
     */
    Response addPerson(Person person);
}
