package net.kenzan.helloworld.domain.api;

import com.google.inject.Inject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kenzan.helloworld.domain.entities.Person;
import net.kenzan.helloworld.domain.responses.FetchPeopleResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path ("v1/people")

@Consumes ({"application/json"})
@Produces ({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Api (
        description = "the v1 API"
)
public class V1ApiRS {

    @Inject
    private V1ApiDelegate delegate;

    public V1ApiRS() {
    }

    @GET
    @Path("/{userId}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation (
            value = "Get people",
            notes = "",
            response = FetchPeopleResponse.class,
            tags = {"helloworld"}
    )
    @ApiResponses ({@ApiResponse (
            code = 200,
            message = "successful operation",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 400,
            message = "Used for a 400 HTTP response - Malformed request",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 401,
            message = "Used for a 401 HTTP response - Client not authenticated",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 403,
            message = "The logged-in user is not permitted to perform the current operation.",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 404,
            message = "Used for a 404 HTTP response - Information or resource not found",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 500,
            message = "Used for a 500 HTTP response - Internal server error",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 200,
            message = "Used for a 500 HTTP response - Internal server error",
            response = FetchPeopleResponse.class
    )})
    public Response delegateGetPeople(@ApiParam ("Session identity information. This contains the SessionIdentityInfo object that has been serialized to JSON and then base64 encoded. Passing of this header is not required for non-domain service client implementations, but should be provided for domain service to domain service communication. ")  @HeaderParam ("X-DOMAIN-SESSION-IDENTITY") String xDomainSessionIdentity, @PathParam ("userId") String userId) {
        return delegate.getPeople(xDomainSessionIdentity, userId);
    }

    @POST
    @Path("/")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation (
            value = "Add new person",
            notes = "",
            response = FetchPeopleResponse.class,
            tags = {"helloworld"}
    )
    @ApiResponses ({@ApiResponse (
            code = 200,
            message = "successful operation",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 400,
            message = "Used for a 400 HTTP response - Malformed request",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 401,
            message = "Used for a 401 HTTP response - Client not authenticated",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 403,
            message = "The logged-in user is not permitted to perform the current operation.",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 404,
            message = "Used for a 404 HTTP response - Information or resource not found",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 500,
            message = "Used for a 500 HTTP response - Internal server error",
            response = FetchPeopleResponse.class
    ), @ApiResponse(
            code = 200,
            message = "Used for a 500 HTTP response - Internal server error",
            response = FetchPeopleResponse.class
    )})
    public Response delegateAddPerson(@ApiParam(value= "Request to create person", required = true) Person person) {
        return delegate.addPerson(person);
    }


}
