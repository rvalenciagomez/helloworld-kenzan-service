/*
 * Copyright 2018, Charter Communications, All rights reserved.
 */
package net.kenzan.helloworld.domain.api.impl;


import javax.ws.rs.core.Response;

import com.google.common.base.Preconditions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.kenzan.helloworld.domain.api.V1ApiDelegate;
import net.kenzan.helloworld.domain.entities.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

import net.kenzan.helloworld.domain.service.ServiceApptsDomainService;

/**
 * @author rvalencia
 */
public class V1ApiDelegateImpl implements V1ApiDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(V1ApiDelegateImpl.class);

    private final ServiceApptsDomainService domainService;

    @Inject
    public V1ApiDelegateImpl (ServiceApptsDomainService domainService) {
        this.domainService = domainService;
    }


    @Override
    public Response getPeople (String xDomainSessionIdentity, String userId) {
        Preconditions.checkNotNull(xDomainSessionIdentity);
        Preconditions.checkNotNull(userId);
        return domainService.getPeople(xDomainSessionIdentity, userId);
    }

    @Override
    public Response addPerson (Person person) {
        Preconditions.checkNotNull(person);
        return domainService.addPerson(person);
    }

}
