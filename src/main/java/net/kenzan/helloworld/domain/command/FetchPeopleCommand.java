/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.command;

import net.kenzan.helloworld.domain.api.PeopleApiV2;
import net.kenzan.helloworld.domain.responses.FetchPeopleResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FetchPeopleCommand extends DomainCommand<FetchPeopleResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchPeopleCommand.class);

    private PeopleApiV2 peopleApi;
    private static String userId;

    public FetchPeopleCommand (
            String userId
    ) {
        super(FetchPeopleCommand.class.getSimpleName());
        peopleApi = new PeopleApiV2();
        this.userId = userId;
    }

    @Override
    protected FetchPeopleResponse run () throws Exception {

        LOGGER.info("userId: {}", userId);

        return peopleApi.queryPeople(userId);
    }

}

