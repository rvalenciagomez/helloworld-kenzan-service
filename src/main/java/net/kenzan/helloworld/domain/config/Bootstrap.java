/*
 * Copyright 2018, Charter Communications, All rights reserved.
 */
package net.kenzan.helloworld.domain.config;

import com.google.inject.Binder;

import net.kenzan.helloworld.domain.config.bootstrap.ServerBootstrap;
import net.kenzan.helloworld.domain.config.module.RestModule;
import net.kenzan.helloworld.domain.config.module.HelloworldModule;


/**
 * starts the service in PROD mode
 */
public class Bootstrap extends ServerBootstrap {

    @Override
    protected void configureBinder(Binder binder) {
        binder.install(new ObjectMapperModule());
        binder.install(new RestModule());
        binder.install(new HelloworldModule());
    }

}