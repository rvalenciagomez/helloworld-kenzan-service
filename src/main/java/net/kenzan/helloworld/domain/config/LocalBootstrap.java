/*
 * Copyright 2018, Charter Communications, All rights reserved.
 */
package net.kenzan.helloworld.domain.config;


import net.kenzan.helloworld.domain.config.bootstrap.ServerBootstrap;
import net.kenzan.helloworld.domain.config.module.LocalServiceapptsModule;
import net.kenzan.helloworld.domain.config.module.RestModule;

import com.google.inject.Binder;


/**
 * starts the service in LOCAL mode. any responses to requests sent in this mode need to be mocked
 **/
public class LocalBootstrap extends ServerBootstrap  {


    protected void configureBinder(Binder binder) {
        binder.install(new ObjectMapperModule());
        binder.install(new RestModule());
        binder.install(new LocalServiceapptsModule());
    }

}
