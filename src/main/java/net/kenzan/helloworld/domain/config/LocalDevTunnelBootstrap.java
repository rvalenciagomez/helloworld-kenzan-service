/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.config;


import net.kenzan.helloworld.domain.config.bootstrap.ServerBootstrap;
import net.kenzan.helloworld.domain.config.module.RestModule;
import net.kenzan.helloworld.domain.config.module.HelloworldModule;
import com.google.inject.Binder;


/**
 * starts the service in LOCAL DEV TUNNEL mode. the service is started locally and expects communication with
 * other services to be facilitated via ssh tunnel
 *
 */
public class LocalDevTunnelBootstrap extends ServerBootstrap {

    @Override
    protected void configureBinder(Binder binder) {
        binder.install(new ObjectMapperModule());
        binder.install(new RestModule());
        binder.install(new HelloworldModule());
    }

}
