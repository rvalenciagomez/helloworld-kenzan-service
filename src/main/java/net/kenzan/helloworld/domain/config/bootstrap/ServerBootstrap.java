package net.kenzan.helloworld.domain.config.bootstrap;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

public class ServerBootstrap {
    public ServerBootstrap() {
    }

    Injector bootstrap() {
        return Guice.createInjector(new Module[] {new GuiceBootstrapModule()});
    }

    protected void configureBinder(Binder binder) {
    }

    protected class GuiceBootstrapModule extends AbstractModule {

        @Override
        protected void configure() {
            ServerBootstrap.this.configureBinder(this.binder());
        }
    }
}
