//package net.kenzan.helloworld.domain.config.bootstrap;
//
//import com.google.inject.AbstractModule;
//import com.google.inject.Binder;
//import com.google.inject.Guice;
//import com.google.inject.Injector;
//import com.google.inject.Module;
//import com.google.inject.TypeLiteral;
//import com.google.inject.servlet.GuiceServletContextListener;
//import com.google.inject.spi.TypeEncounter;
//import com.google.inject.spi.TypeListener;
//import net.kenzan.helloworld.domain.config.LocalBootstrap;
//import net.kenzan.helloworld.domain.config.ObjectMapperModule;
//import net.kenzan.helloworld.domain.config.module.LocalServiceapptsModule;
//import net.kenzan.helloworld.domain.config.module.RestModule;
//
//public class ServerBootstrapContextListener implements Module, TypeListener {
//
//    private static ServerBootstrapContextListener listener = new ServerBootstrapContextListener();
//
//    public static ServerBootstrapContextListener me() {
//        return listener;
//    }
//
//    private Injector injector;
//
//    private ServerBootstrapContextListener () {
//        injector = Guice.createInjector(this);
//    }
//
//    protected Injector getInjector () {
//        return injector;
//    }
//
//    @Override
//    public void configure (Binder binder) {
//        binder.install(new ObjectMapperModule());
//        binder.install(new RestModule());
//        binder.install(new LocalServiceapptsModule());
//    }
//
//    @Override
//    public <I> void hear (TypeLiteral<I> type, TypeEncounter<I> encounter) {
//
//    }
//}
