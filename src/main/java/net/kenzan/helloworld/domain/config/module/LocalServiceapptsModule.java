/*
 * Copyright 2018, Charter Communications, All rights reserved.
 */
package net.kenzan.helloworld.domain.config.module;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import net.kenzan.helloworld.domain.api.V1ApiDelegate;
import net.kenzan.helloworld.domain.api.impl.V1ApiDelegateImpl;
import net.kenzan.helloworld.domain.dao.EnterprisePeopleDao;
import net.kenzan.helloworld.domain.dao.stub.EnterprisePeopleDaoStub;
import net.kenzan.helloworld.domain.service.ServiceApptsDomainService;
import net.kenzan.helloworld.domain.service.impl.ServiceApptsDomainServiceImpl;

public class LocalServiceapptsModule extends AbstractModule {
    @Override
    protected void configure () {
        bind(V1ApiDelegate.class).to(V1ApiDelegateImpl.class).in(Scopes.SINGLETON);
        bind(ServiceApptsDomainService.class).to(ServiceApptsDomainServiceImpl.class).in(Scopes.SINGLETON);
        bind(EnterprisePeopleDao.class).to(EnterprisePeopleDaoStub.class).in(Scopes.SINGLETON);
    }
}
