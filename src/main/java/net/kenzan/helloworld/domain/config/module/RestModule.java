/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.config.module;

import java.util.HashMap;

import com.google.inject.Scopes;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import net.kenzan.helloworld.domain.security.context.DomainServiceExceptionMapper;

/**
 * The {@link RestModule} is a Guice module that configures the appropriate
 * JAX-RS resources and serves them under a given path.
 */
public class RestModule extends JerseyServletModule {

//    private static final String FACTORIES = TxnLogFilterFactory.class.getName();
    private static final String PACKAGES = "net.kenzan.helloworld.domain.api net.kenzan.helloworld.domain.config net.spectrum.portal.healthcheck";

    @Override
    protected void configureServlets() {
        HashMap<String, String> initParams = new HashMap<>();
//        initParams.put(ResourceConfig.PROPERTY_RESOURCE_FILTER_FACTORIES, FACTORIES);
        initParams.put(PackagesResourceConfig.PROPERTY_PACKAGES, PACKAGES);
        serve("/*").with(GuiceContainer.class, initParams);
        bind(DomainServiceExceptionMapper.class).in(Scopes.SINGLETON);
    }

}
