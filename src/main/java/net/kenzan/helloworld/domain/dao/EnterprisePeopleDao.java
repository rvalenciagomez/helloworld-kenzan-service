/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.dao;


import javax.ws.rs.core.Response;


public interface EnterprisePeopleDao {

    Response fetchPeople(String userId);
}
