/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.dao.impl;


import net.kenzan.helloworld.domain.command.FetchPeopleCommand;
import net.kenzan.helloworld.domain.dao.EnterprisePeopleDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;


public class EnterprisePeopleDaoImpl implements EnterprisePeopleDao {

    Logger LOGGER = LoggerFactory.getLogger(EnterprisePeopleDaoImpl.class);

    public EnterprisePeopleDaoImpl () {
    }

    @Override
    public Response fetchPeople (String userId) {
        return Response.ok(
                new FetchPeopleCommand(userId)
                        .observe()
                        .toBlocking()
                        .single()
            ).build();
    }
}



