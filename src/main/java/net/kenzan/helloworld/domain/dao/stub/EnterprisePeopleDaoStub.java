/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.dao.stub;


import com.google.gson.Gson;
import net.kenzan.helloworld.domain.dao.EnterprisePeopleDao;
import net.kenzan.helloworld.domain.responses.FetchPeopleResponse;
import net.kenzan.helloworld.domain.entities.DomainResultCode;
import net.kenzan.helloworld.domain.entities.Person;
import net.kenzan.helloworld.domain.helpers.TransactionHelper;

import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;


public class EnterprisePeopleDaoStub implements EnterprisePeopleDao {

    private static final String RESULT_MESSAGE = DomainResultCode.SUCCESS.toString();

    public static final String APPT_ID_FOR_EMPTY = "666";


    @Override
    public Response fetchPeople (String userId) {

        List<Person> people = Arrays.asList(
                    new Person("Rob", "rval@gmail.com", "1231", LocalDate.now()),
                    new Person("John", "john@gmail.com", "12345", LocalDate.now()),
                    new Person("Scott", "scott@gmail.com", "3345", LocalDate.now())
                );

        String peopleJson = new Gson().toJson(people);
        System.out.println("peopleJson -->" + peopleJson);

        return Response.ok(
                new FetchPeopleResponse.Builder()
                                       .withPeople(people)
                                       .withCode(DomainResultCode.SUCCESS)
                                       .withMessage("SUCCESS")
                                       .withTimestamp(OffsetDateTime.now())
                                       .withTxnId(TransactionHelper.getTransactionHeader())
                                       .build()
        ).build();
    }

}
