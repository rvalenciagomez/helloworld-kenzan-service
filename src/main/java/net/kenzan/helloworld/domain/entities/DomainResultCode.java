package net.kenzan.helloworld.domain.entities;

import java.util.Arrays;
import java.util.function.Predicate;

public enum DomainResultCode {
    BAD_REQUEST(400),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    NO_DATA_FOUND(404),
    NOT_ALLOWED(405),
    NOT_ACCEPTABLE(406),
    CONFLICT(409),
    PRECONDITION_FAILED(412),
    UNPROCESSABLE_ENTITY(422),
    TOO_MANY_REQUESTS(429),
    SERVER_ERROR(500),
    NOT_IMPLEMENTED(501),
    BAD_GATEWAY(502),
    SERVICE_UNAVAILABLE(503),
    SUCCESS(200),
    NO_CONTENT(204);

    private int status;

    private DomainResultCode(int status) {
        this.status = status;
    }

    public int status() {
        return this.status;
    }

    public static DomainResultCode fromValue(String value) {
        return (DomainResultCode)Arrays.stream(values()).filter((domainResultCode) -> {
            return domainResultCode.name().equalsIgnoreCase(value);
        }).findAny().orElse(SERVER_ERROR);
    }

    public static DomainResultCode fromIntValue(int value) {
        return (DomainResultCode)Arrays.stream(values()).filter((domainResultCode) -> {
            return domainResultCode.status() == value;
        }).findAny().orElse(SERVER_ERROR);
    }

    public static DomainResultCode fromIntValue(Integer value) {
        return (DomainResultCode)Arrays.stream(values()).filter((domainResultCode) -> {
            return domainResultCode.status() == value;
        }).findAny().orElse(SERVER_ERROR);
    }
}
