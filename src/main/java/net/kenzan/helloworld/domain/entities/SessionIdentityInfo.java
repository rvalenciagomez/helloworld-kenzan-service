package net.kenzan.helloworld.domain.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;

import java.util.Map;
import java.util.Objects;

/**
 * @author eoliveira
 */
public class SessionIdentityInfo {

    private final String accountNumber;

    private final String guid;

    private final String username;

    private final String clientIpAddress;

    private final String sessionId;


    private SessionIdentityInfo(
            String accountNumber,
            String guid,
            String username,
            String clientIpAddress,
            String sessionId) {

        this.accountNumber = accountNumber;
        this.guid = guid;
        this.username = username;
        this.clientIpAddress = clientIpAddress;
        this.sessionId = sessionId;
    }

    @JsonCreator
    public static SessionIdentityInfo create(
            @JsonProperty("accountNumber") String accountNumber,
            @JsonProperty("guid") String guid,
            @JsonProperty("username") String username,
            @JsonProperty("clientIpAddress") String clientIpAddress,
            @JsonProperty("sessionId") String sessionId) {

        return new SessionIdentityInfo.Builder()
                .withAccountNumber(accountNumber)
                .withGuid(guid)
                .withUsername(username)
                .withClientIpAddress(clientIpAddress)
                .withSessionId(sessionId)
                .build();

    }

    /**
     * The account number for the calculated footprint
     * @return accountNumber
     **/
    public String getAccountNumber() {

        return accountNumber;
    }

    /**
     * The GUID of the member for the calculated footprint
     * @return guid
     **/
    public String getGuid() {

        return guid;
    }

    /**
     * The user name of who owns the session
     * @return username
     **/
    public String getUsername() {
        return username;
    }

    /**
     * Address added by the ELB, needed by some legacy endpoints
     * @return clientIpAddress
     **/
    public String getClientIpAddress() {

        return clientIpAddress;
    }

    /**
     * The session key from which the user info was generated. This value is necessary for any passthrough to Legacy Charter
     * @return sessionId
     **/
    public String getSessionId() {

        return sessionId;
    }

    @Override
    public boolean equals(java.lang.Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SessionIdentityInfo sessionIdentityInfo = (SessionIdentityInfo) o;

        return Objects.equals(this.accountNumber, sessionIdentityInfo.accountNumber) &&
                Objects.equals(this.guid, sessionIdentityInfo.guid) &&
                Objects.equals(this.username, sessionIdentityInfo.username) &&
                Objects.equals(this.clientIpAddress, sessionIdentityInfo.clientIpAddress) &&
                Objects.equals(this.sessionId, sessionIdentityInfo.sessionId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(accountNumber, guid, username, clientIpAddress, sessionId);
    }


    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("class SessionIdentityInfo {\n");

        sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
        sb.append("    guid: ").append(toIndentedString(guid)).append("\n");
        sb.append("    username: ").append(toIndentedString(username)).append("\n");
        sb.append("    clientIpAddress: ").append(toIndentedString(clientIpAddress)).append("\n");
        sb.append("    sessionId: ").append(toIndentedString(sessionId)).append("\n");
        sb.append("}");

        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public String toJson() {

        return new Gson().toJson(this);
    }

    public SessionIdentityInfo fromJson(String json) {

        return new Gson().fromJson(json, SessionIdentityInfo.class);
    }

    public static class Builder {

        private String accountNumber;
        private String guid;
        private String username;
        private String clientIpAddress;
        private String sessionId;

        public Builder withAccountNumber(String accountNumber) {

            this.accountNumber = accountNumber;
            return this;
        }

        public Builder withGuid(String guid) {

            this.guid = guid;
            return this;
        }

        public Builder withUsername(String username) {

            this.username = username;
            return this;
        }

        public Builder withClientIpAddress(String clientIpAddress) {

            this.clientIpAddress = clientIpAddress;
            return this;
        }

        public Builder withSessionId(String sessionId) {

            this.sessionId = sessionId;
            return this;
        }

        public SessionIdentityInfo build() {

            return new SessionIdentityInfo(
                    this.accountNumber,
                    this.guid,
                    this.username,
                    this.clientIpAddress,
                    this.sessionId);
        }

        public Builder fromExisting(SessionIdentityInfo existing) {

            Builder builder = new SessionIdentityInfo.Builder();
            builder.withAccountNumber(existing.getAccountNumber());
            builder.withGuid(existing.getGuid());
            builder.withUsername(existing.getUsername());
            builder.withClientIpAddress(existing.getClientIpAddress());
            builder.withSessionId(existing.getSessionId());
            return builder;
        }
    }

}