package net.kenzan.helloworld.domain.helpers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Base64;
import java.util.concurrent.Callable;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;

public class Decoder {
    private final ObjectMapper mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(Decoder.class);

    public Decoder() {
        this.mapper = new ObjectMapper();
        this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public Decoder(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public <T> T decode(String encoded, Class<T> target) {
        try {
            return this.mapper.readValue(Base64.getDecoder().decode(encoded), target);
        } catch (Exception var5) {
            String errorMessage = "Error decoding" + encoded + "to class" + target.getName();
            LOGGER.error(errorMessage + ": {}", var5);
            throw new RuntimeException(String.format("Error decoding %s to class %s", encoded, target.getName()), var5);
        }
    }

    public String encode(String decoded) {
        try {
            return Base64.getEncoder().encodeToString(decoded.getBytes());
        } catch (Exception ex) {
            String errorMessage = "Error encoding" + decoded;
            LOGGER.error(errorMessage + ": {}", ex);
            throw new RuntimeException(String.format("Error encoding %s", decoded), ex);
        }
    }

    public <T> Observable<T> decodeToObservable(String encoded, Class<T> target) {
        return Observable.fromCallable(() -> {
            return this.decode(encoded, target);
        });
    }
}