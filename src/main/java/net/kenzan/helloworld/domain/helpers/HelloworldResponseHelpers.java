package net.kenzan.helloworld.domain.helpers;

import com.sun.jersey.api.client.ClientResponse;
import net.kenzan.helloworld.domain.responses.CommonApiResponse;
import net.kenzan.helloworld.domain.responses.HelloworldResponse;
import net.kenzan.helloworld.domain.entities.DomainResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.Optional;

/**
 * helpers to deal with common Helloworld Response munging
 */
public class HelloworldResponseHelpers {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloworldResponseHelpers.class);


    /**
     * @return
     * @implNote the TxnId field from the slf4j MDC -- will include a blank string in the message field if one isn't
     * supplied
     */
    public static CommonApiResponse getCommonApiResponse (Optional<String> messageOptional) {
        String message = messageOptional.orElse("");

        CommonApiResponse.Builder commonApiResponseBuilder =
                new CommonApiResponse.Builder()
                        .withTimestamp(OffsetDateTime.now())
                        .withTxnId(TransactionHelper.getTransactionHeader())
                        .withMessage(message);

        CommonApiResponse commonApiResponse = commonApiResponseBuilder.build();
        return commonApiResponse;
    }

    /**
     * @param messageOptional
     * @param responseStatus
     * @return
     */
    public static Response getResponseWithCommonApiResponse (Optional<String> messageOptional,
                                                             @NotNull Response.Status responseStatus) {

        CommonApiResponse commonApiResponse = getCommonApiResponse(messageOptional);

        Response populatedResponse = Response.status(responseStatus)
                .entity(commonApiResponse)
                .build();

        return populatedResponse;
    }

    /**
     * @param messageOptional
     * @param serviceapptsResultCode
     * @return
     */
    public static HelloworldResponse getHelloworldResponse (Optional<String> messageOptional,
                                                            @NotNull DomainResultCode serviceapptsResultCode) {

        String message = messageOptional.orElse("");
        OffsetDateTime timeStamp = OffsetDateTime.now();
        String txnId = TransactionHelper.getTransactionHeader();

        HelloworldResponse serviceapptsResponse = new HelloworldResponse.Builder()
                .withMessage(message)
                .withTimestamp(timeStamp)
                .withTxnId(txnId)
                .withCode(serviceapptsResultCode)
                .build();
        return serviceapptsResponse;
    }

    public static Optional<Response> getErrorResponseOrNull(ClientResponse clientResponse) {

        int clientStatusCode = clientResponse.getStatus();

        Response.Status clientResponseStatus =
                Response.Status.fromStatusCode(clientStatusCode);

        Response.Status.Family returnCodeFamily =
                Response.Status
                        .fromStatusCode(clientStatusCode)
                        .getFamily();

        Optional<Response> errorResponseOptional = Optional.empty();

        if (returnCodeFamily != Response.Status.Family.SUCCESSFUL) {
            LOGGER.error("something went wrong getting open service appointments for this caller!");


            errorResponseOptional = Optional.of(
                    getResponseWithCommonApiResponse(Optional.empty(), clientResponseStatus)
            );
        }

        return errorResponseOptional;
    }



}