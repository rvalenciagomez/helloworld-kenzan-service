package net.kenzan.helloworld.domain.security.context;

import net.kenzan.helloworld.domain.entities.DomainResultCode;

public class DomainServiceException extends RuntimeException {
    private DomainResultCode domainResultCode;
    private String errorCode;
    private String callTrace;

    public DomainServiceException(String message, DomainResultCode domainResultCode) {
        this(message, (Throwable)null, (DomainResultCode)domainResultCode);
    }

    public DomainServiceException(String message, Throwable cause, DomainResultCode domainResultCode) {
        this(message, cause, domainResultCode, domainResultCode != null ? domainResultCode.name() : null, (String)null);
    }

    public DomainServiceException(String message, DomainResultCode domainResultCode, String errorCode) {
        this(message, (Throwable)null, domainResultCode, errorCode, (String)null);
    }

    public DomainServiceException(String message, Throwable cause, DomainResultCode domainResultCode, String errorCode) {
        this(message, cause, domainResultCode, errorCode, (String)null);
    }

    public DomainServiceException(String message, Throwable cause, DomainResultCode domainResultCode, String errorCode, String callTrace) {
        super(message, cause);
        this.domainResultCode = domainResultCode;
        this.errorCode = errorCode;
        this.callTrace = callTrace;
    }

    public DomainResultCode getDomainResultCode() {
        return this.domainResultCode;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public String getCallTrace() {
        return this.callTrace;
    }

    @Override
    public String toString () {
        return "DomainServiceException{" +
                "domainResultCode=" + domainResultCode +
                ", errorCode='" + errorCode + '\'' +
                ", callTrace='" + callTrace + '\'' +
                '}';
    }
}
