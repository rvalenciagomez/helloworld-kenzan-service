package net.kenzan.helloworld.domain.security.context;

import net.kenzan.helloworld.domain.entities.DomainResultCode;
import net.kenzan.helloworld.domain.helpers.TransactionHelper;
import net.kenzan.helloworld.domain.responses.HelloworldResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.time.OffsetDateTime;
import java.util.Optional;

@Provider
public class DomainServiceExceptionMapper implements ExceptionMapper<DomainServiceException> {

    Logger LOGGER = LoggerFactory.getLogger(DomainServiceException.class);

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";
    private static final String ERROR_MSG = "Something went wrong - {}";

    @Override
    public Response toResponse (DomainServiceException ex) {

        LOGGER.error(ERROR_MSG);

        LOGGER.info("DomainServiceException info --> {}", ex.toString());

        final DomainResultCode domainResultCode = Optional.ofNullable(ex.getDomainResultCode()).orElse(DomainResultCode.BAD_REQUEST);

        HelloworldResponse helloworldResponse = new HelloworldResponse.Builder()
                .withCode(domainResultCode)
                .withMessage(!ex.getMessage().isEmpty() ? ex.getMessage() : ERROR_MSG)
                .withTimestamp(OffsetDateTime.now())
                .withTxnId(TransactionHelper.getTransactionHeader())
                .build();

        return Response
                .status(400)
                .header(CONTENT_TYPE, APPLICATION_JSON)
                .entity(helloworldResponse)
                .build();
    }
}
