/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.service;


import net.kenzan.helloworld.domain.entities.Person;

import javax.ws.rs.core.Response;


/**
 *
 * @author rvalencia
 */
public interface ServiceApptsDomainService {

    /**
     * Authenticates a given identity
     *
     * @param xDomainSessionIdentity
     * @return Response
     */
    Response getPeople(String xDomainSessionIdentity, String userId);

    /**
     * Authenticates a given identity
     *
     * @param person
     * @return Response
     */
    Response addPerson(Person person);

}
