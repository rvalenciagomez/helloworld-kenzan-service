/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.service.impl;


import static net.kenzan.helloworld.domain.helpers.HelloworldResponseHelpers.*;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.kenzan.helloworld.domain.dao.EnterprisePeopleDao;
import net.kenzan.helloworld.domain.entities.Person;
import net.kenzan.helloworld.domain.responses.FetchPeopleResponse;
import net.kenzan.helloworld.domain.entities.SessionIdentityInfo;
import net.kenzan.helloworld.domain.helpers.Decoder;
import net.kenzan.helloworld.domain.service.ServiceApptsDomainService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;


/**
 * business logic layer for helloworld
 */
public class ServiceApptsDomainServiceImpl implements ServiceApptsDomainService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceApptsDomainServiceImpl.class);

    private final EnterprisePeopleDao enterprisePeopleDao;
    private final Decoder decoder;

    /**
     * @param objectMapper
     * @param enterprisePeopleDao
     */
    @Inject
    public ServiceApptsDomainServiceImpl(
            @Named("ResponseObjectMapper") ObjectMapper objectMapper,
            EnterprisePeopleDao enterprisePeopleDao) {

        this.enterprisePeopleDao = enterprisePeopleDao;
        this.decoder = new Decoder();
    }


    @Override
    public Response getPeople(String xDomainSessionIdentity, String userId) {

        SessionIdentityInfo sessionIdentityInfo = decoder.decode(xDomainSessionIdentity, SessionIdentityInfo.class);
        LOGGER.info("getPeople - sessionIdentityInfo: {}", sessionIdentityInfo.toJson());

        if(sessionIdentityInfo.getAccountNumber() == null || sessionIdentityInfo.getGuid() == null) {
            LOGGER.error("Error due to accountNumber or Guid empty in sessionIdentityInfo");
            return getResponseWithCommonApiResponse(Optional.empty(), Response.Status.INTERNAL_SERVER_ERROR);
        }

        Response serviceResponse = enterprisePeopleDao.fetchPeople(userId);

        if (serviceResponse == null) {
            LOGGER.error("Failure in enterpriseWorkOrderImpl.getRescheduleAvailability response");
            return getResponseWithCommonApiResponse(Optional.empty(), Response.Status.INTERNAL_SERVER_ERROR);
        }

        if(serviceResponse.getEntity() instanceof FetchPeopleResponse) {
            FetchPeopleResponse fetchPeopleResponse = (FetchPeopleResponse) serviceResponse.getEntity();

            return fetchPeopleResponse.getPeople().isEmpty()
                    ? getResponseWithCommonApiResponse(Optional.empty(), Response.Status.NOT_FOUND)
                    : serviceResponse;
        }
        return serviceResponse;
    }

    @Override
    public Response addPerson (Person person) {
        return createPerson(person);
    }



    private Response createPerson(Person person) {
        Client client = new Client();
        WebResource webResource = client.resource("'https://reqres.in/api/users");

        String personJsonInput = new Gson().toJson(person);

        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, personJsonInput);

        LOGGER.info("client response with status: {}", clientResponse.getStatus());

        if(clientResponse.getStatus() != 200 ) {
            LOGGER.error("There was some error");
            return getResponseWithCommonApiResponse(Optional.empty(), Response.Status.INTERNAL_SERVER_ERROR);
        }

        return Response.ok(clientResponse).build();
    }


}
