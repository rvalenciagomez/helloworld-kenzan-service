/*
 * Copyright 2018, Charter Communications,  All rights reserved.
 */
package net.kenzan.helloworld.domain.api;


import com.google.inject.Inject;
import net.kenzan.helloworld.domain.dao.EnterprisePeopleDao;
import net.kenzan.helloworld.domain.dao.stub.EnterprisePeopleDaoStub;
import net.kenzan.helloworld.domain.helpers.Decoder;
import net.kenzan.helloworld.domain.responses.FetchPeopleResponse;
import net.kenzan.helloworld.domain.service.impl.ServiceApptsDomainServiceImpl;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.junit.Assert.*;

import net.kenzan.helloworld.domain.responses.HelloworldResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.ws.rs.core.Response;


@RunWith(MockitoJUnitRunner.class)
public class HelloWorldKenzanServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldKenzanServiceTest.class);

    @InjectMocks
    private Decoder decoder;

    private static final String TXNID_KEY = "txnId";
    private static final String TXNID_VALUE = "fake_txnId";
    private static final String USER_ID = "fake_userId";
    private static final String IDENTITY_INFO = "{\"footprint\":null,\"clientIpAddress\":\"172.0.0.1\",\"sessionId\":\"1234567\", \"guid\":\"12345\",\"accountFootprintMap\":{}, \"accountNumber\":\"8087300020036003\"}";
    private String ENCODED_IDENTITY_INFO;

    @Mock
    private EnterprisePeopleDao enterprisePeopleDao;

    @Mock
    private ObjectMapper mapper;

    private EnterprisePeopleDaoStub enterprisePeopleDaoStub;
    private ServiceApptsDomainServiceImpl serviceApptsService;


    @Before
    public void init() {

        ENCODED_IDENTITY_INFO = decoder.encode(IDENTITY_INFO);

        // MDC = Mapped Diagnostic Context
        // https://logback.qos.ch/manual/mdc.html
        MDC.put(TXNID_KEY, TXNID_VALUE);

        enterprisePeopleDaoStub = new EnterprisePeopleDaoStub();

        serviceApptsService = new ServiceApptsDomainServiceImpl(mapper,
                enterprisePeopleDao);
    }

    @Test
    public void getRescheduleAvailabilityTest() {

        when(enterprisePeopleDao.fetchPeople(USER_ID))
                .thenReturn(enterprisePeopleDaoStub.fetchPeople(USER_ID));

        Response response = serviceApptsService.getPeople(ENCODED_IDENTITY_INFO, USER_ID);

        assertNotNull(response.getEntity());
        assertTrue(response.getEntity() instanceof HelloworldResponse);

        FetchPeopleResponse fetchPeopleResponse = (FetchPeopleResponse) response.getEntity();

        assertEquals("SUCCESS", fetchPeopleResponse.getMessage());
        assertEquals(3, fetchPeopleResponse.getPeople().size());
    }

//    @Test(expected = Exception.class)
//    public void getRescheduleAvailabilityExceptionTest() throws Exception {
//
//        when(workOrderService.getRescheduleAvailability(Mockito.any()))
//                .thenThrow(new Exception("SAMPLE_ERROR"));
//
//        serviceApptsService.getPeople(ENCODED_IDENTITY_INFO);
//    }

//    @Test
//    public void getRescheduleAvailabilityIsEmptyTest() {
//
//        when(enterprisePeopleDao.fetchPeople())
//                .thenReturn(enterprisePeopleDaoStub.fetchPeople());
//
//        Response response = serviceApptsService.getPeople(ENCODED_IDENTITY_INFO);
//
//        assertNotNull(response.getEntity());
//        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
//    }

}
